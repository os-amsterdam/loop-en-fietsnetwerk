# SpatialNetworkAmsterdam

This repo is intended to demonstrate the use and analysis of a spatial network of Amsterdam.
The network is suitable for analysis concerning pedestrians and cyclists.
Examples are provided in R.

---

## system requirements

The code within this tutorial runs on a `4+` version of `R` and requires having the following packages installed:

* `rmarkdown`
* `tidyverse`
* `data.table`
* `sf`
* `sfnetworks`
* `dodgr`
* `mapview`
* `leafsync`
